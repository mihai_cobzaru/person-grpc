package service;

import io.grpc.stub.StreamObserver;
import proto.PersonGrpc;
import proto.PersonOuterClass;

public class Person extends PersonGrpc.PersonImplBase {

    @Override
    public void sendPersonData(PersonOuterClass.PersonRequest request, StreamObserver<PersonOuterClass.PersonReply> responseObserver) {
        //super.sendPersonData(request, responseObserver);

        System.out.println(request);
        int type = Character.getNumericValue(request.getCnp().charAt(0));
        String cnp = request.getCnp();

        parseInput(request,cnp,type);

        PersonOuterClass.PersonReply reply = PersonOuterClass.PersonReply.newBuilder().setMessage("Reply generat cu succes!").build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

    private static void parseInput( PersonOuterClass.PersonRequest request, String cnp, int type) {

        if(type == 1 || type == 5) {

            System.out.println(request.getName());
            if( type == 1) {
                System.out.println("Barbat nascut in " + cnp.charAt(5) + cnp.charAt(6) + "/" + cnp.charAt(3) + cnp.charAt(4)
                        + "/19" + cnp.charAt(1) + cnp.charAt(2));
            } else {

                System.out.println("Barbat nascut in " + cnp.charAt(5) + cnp.charAt(6) + "/" + cnp.charAt(3) + cnp.charAt(4)
                        + "/20" + cnp.charAt(1) + cnp.charAt(2));

            }
        }

        if(type == 2 || type == 6) {

            System.out.println(request.getName());
            if(type == 2) {

                System.out.println("Femeie nascuta in " + cnp.charAt(5) + cnp.charAt(6) + "/" + cnp.charAt(3) + cnp.charAt(4)
                        + "/19" + cnp.charAt(1) + cnp.charAt(2));
            } else {

                System.out.println("Femeie nascuta in " + cnp.charAt(5) + cnp.charAt(6) + "/" + cnp.charAt(3) + cnp.charAt(4)
                        + "/20" + cnp.charAt(1) + cnp.charAt(2));
            }
        }
    }
}
