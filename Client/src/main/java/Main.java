import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.PersonGrpc;
import proto.PersonOuterClass;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        PersonGrpc.PersonBlockingStub personStub = PersonGrpc.newBlockingStub(channel);

        boolean running = true;
        while(running) {

            Scanner sc = new Scanner(System.in);
            System.out.println("Introduceti numele si Cnp ul dumneavoastra: ");
            String name = sc.nextLine();
            String cnp = sc.nextLine();

            if(validateCnp(cnp) && validateName(name)) {

                personStub.sendPersonData(PersonOuterClass.PersonRequest.newBuilder().setCnp(cnp).setName(name).build());
                System.out.println("Mesajul a fost transmis cu succes!\n");
            } else {

                System.out.println("A fost o eroare la transmiterea mesajului\n");
            }

        }


        channel.shutdown();
    }

    private static boolean validateCnp(String cnp) {
        final int CNP_LENGTH = 13;

        boolean validated = false;
        boolean dateCondition = (Integer.parseInt(cnp.substring(3,5)) < 13) && (Integer.parseInt(cnp.substring(5,7)) < 32);

        if(cnp.length() != CNP_LENGTH) {
            return false;
        }

        if((cnp.charAt(0) == '1' || cnp.charAt(0) == '2' || cnp.charAt(0) == '5' || cnp.charAt(0) == '6') && dateCondition) {

            Pattern pattern = Pattern.compile("[^0-9]", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(cnp);

            boolean matchFound = matcher.find();
            if(matchFound) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    private static boolean validateName(String name) {

        Pattern pattern = Pattern.compile("[^a-z ]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);
        boolean matchFound = matcher.find();

        if(matchFound) {

            return false;
        }

        return true;
    }
}
